import random
from pygame.math import Vector2

import core
import math

colorFamily = [(255, 255, 255), (0, 255, 0), (255, 0, 0)]


class AGENT:
    def __init__(self, family=0):
        self.debug = False
        self.pos = Vector2(random.randint(0, 700), random.randint(0, 700))
        self.vel = Vector2(random.uniform(-5, 5), random.uniform(-5, 5))
        self.acc = Vector2()
        self.family = family
        self.maxAcc = 1
        self.maxSpeed = 4
        self.perception = 100
        self.separationFactor = 1
        self.alignFactor = 0.1
        self.cohesionFactor = 1
        self.se = Vector2()
        self.co = Vector2()
        self.radiG = 5
        self.radib = 5

    def update(self):
        if self.acc.length() > self.maxAcc:
            self.acc.scale_to_length(self.maxAcc)

        self.vel += self.acc
        if self.vel.length() > self.maxSpeed:
            self.vel.scale_to_length(self.maxSpeed)
        self.acc = Vector2(0, 0)
        self.pos += self.vel

    def edge(self, sizes):
        if self.pos.x < 0:
            self.pos.x = sizes[0]
        if self.pos.x > sizes[0]:
            self.pos.x = 0
        if self.pos.y < 0:
            self.pos.y = sizes[1]
        if self.pos.y > sizes[1]:
            self.pos.y = 0

    def show(self):

        core.Draw.circle(colorFamily[self.family], self.pos, self.radib)

        if self.debug:
            core.Draw.line((255, 255, 255), self.pos, self.pos)
            core.Draw.line((255, 0, 0), self.pos, self.pos)

    def showG(self):

        core.Draw.circle(colorFamily[self.family], self.pos, self.radiG)

        if self.debug:
            core.Draw.line((255, 255, 255), self.pos, self.pos)
            core.Draw.line((255, 0, 0), self.pos, self.pos)

    def mange(self, Gboids, boids):

        for gb in Gboids:

            gx = gb.pos[0]
            gy = gb.pos[1]
            for bo in boids:
                bx = bo.pos[0]
                by = bo.pos[1]
                dis = math.sqrt((gx - bx)**2 + (gy - by)**2)

                if dis <= gb.radiG + bo.radib:
                    gb.family = bo.family
