import random
import core
from agent import AGENT


def setup():
    core.fps = 30
    core.WINDOW_SIZE = [700, 700]
    core.memory("malad", [])
    core.memory("maladNb", 30)
    core.memory("nonmalad", [])
    core.memory("nonmaladNb", 30)

    for i in range(0, core.memory("maladNb")):
        core.memory("malad").append(AGENT(2))
    for i in range(0, core.memory("nonmaladNb")):
        core.memory("nonmalad").append(AGENT(0))


def run():
    core.cleanScreen()
    for b in core.memory("malad"):
        b.update()
        b.show()
        b.edge(core.WINDOW_SIZE)

    for b in core.memory("nonmalad"):
        b.mange(core.memory("nonmalad"), core.memory("malad"))
        b.update()
        b.showG()
        b.edge(core.WINDOW_SIZE)


core.main(setup, run)